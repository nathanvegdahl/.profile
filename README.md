# About me

Hi!  I'm Nathan Vegdahl.

I'm a Blender developer at the Blender Institute, currently working primarily on the Animation 2025 project.  I also dabble in color management, rendering, and VFX-related development from time to time.

In the past I worked as both an animator and character rigger on the Blender open movie projects [Big Buck Bunny](https://peach.blender.org) and [Sintel](https://durian.blender.org/).  I also worked as a UX designer and usability testing engineer at Nintendo Technology Development, which is significantly less impressive than it sounds.

それに少し日本語ができます。ですからもし英語がよく分からなかったら、日本語で連絡してもいいですよ！

# Contact

- E-mail: nathan@blender.org
- blender.chat: [cessen](https://blender.chat/direct/cessen)

# Misc

- [Weekly development reports](https://projects.blender.org/nathanvegdahl/.profile/src/branch/main/reports)
- [My hobby renderer blog](https://psychopath.io)