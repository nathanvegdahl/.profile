# Project Setup

Reference: [#133001](https://projects.blender.org/blender/blender/issues/133001)

## 1. Blender Variables

Primary use case for this stage: path substitution.  E.g. for render output:

```
//../../renders/${blend_file_name}/frame_####.png
```

After talking to Bastien, it sounds like there are a few possible ways to approach this:

- **Collaborate with overrides**, and implement it in those terms.
    - Pros: can be re-used for general project variables later (i.e. not specific to paths).
    - Cons: the overrides system needs work before it would be ready for this, so not a short-term target.
- **Path-specific implementation.**
    - Pros: can start work on it right away.
    - Cons: according to Bastien there's a good chance it will be harder than it looks, and may be a time sink to really get working properly.  Would be bespoke just for paths, and wouldn't be consistent with how projects variables work with other property types in the future.
- **Store separate expressions per property**, which are then evaluated.  A bit like drivers, but a different system not limited to animatable properties.
    - Pros: general system for handling variables.
    - Cons: a relatively invasive and bespoke system just to allow us to handle variables.  Dunno how much work it would be.
- **Project-level auto-execute-on-blend-file-load script**, with access to these variables.
    - Pros: we probably want this anyway, as it gives a powerful "escape hatch" for projects to do anything that we don't directly support through other features.  Probably simple to implement, and can be accomplished on a short timeline.
    - Cons(?): requires project infrastructure to work in a project-like context, so we'll likely want to implement minimal project features first.

**Proposal:**

Let's do the last option (auto-execute-script).

- Expose these variables to RNA and the Python API.
- Make them available in drivers as well.
- When we have Projects, allow specifying Python scripts that should be run on load.

**Update:**

Talked to Julian and Dalai.

Julian was like, "Yeah!  Totally makes sense!"

Dalai was like, "Well, right now this is our discovery phase: figure out what is even feasible.  So let's look into how feasible the more full implementation of variables is first, and then we can figure out what to do from there.  Talk to Brecht, I think he had some ideas for how variables could work."

### Conclusion

After talking to Brecht, and then Dalai again, and then the studio artists:
- **File paths are the main real use case, so just do those first as their own thing.**  There are probably good ways to handle the other cases as their own feature.

Some probably relevant files:
- `path_utils.cc`
- `BLI_path_utils.hh`

The function that turns `//` into a full filepath is `BLI_path_abs()`.  There's also `BLI_path_abs_from_cwd()` which expands normal relative paths (that don't start with a slash at all) in the normal way.  If I track down the uses of these, then *probably* those are the places where we would need to do variable substitution as well.

Questions:
- How should use of these variables work in the context of linked data from other blend files?  I would guess however `//` works (so probably using variables as they would be in the particular library file the data comes from?).  But should double-check that this makes sense.
- Is there any kind of escaping logic in path processing already?  If not, how should escaping work? Backslash is already accepted as-is as a path separator, so we can't use that.
- What to do when encountering a variable that isn't defined?

### Time Estimate

- 1-2 days for exploratory coding to figure out how to approach this.
- 5 days to actually implement the thing.
- Total: (2 days + 5 days) * 2 for buffer = 14 days.


## 2. Project UI + Project Awareness

Just building a basic "create project" UI, with project name.  No settings beyond that.

Also adding a "project root" variable for use in paths.

We potentially have a bit of a head start on this thanks to Julian's old PR: [#107655: WIP: Basic Blender Project Support (experimental feature)](https://projects.blender.org/blender/blender/pulls/107655)

### Time Estimate

- 1 day to review [#107655](https://projects.blender.org/blender/blender/pulls/107655) more closely and see what I can reuse from that.
- 1 day to look into doing TOML from C++, or if we can just do all the
TOML stuff from Python (take a look at how extensions does it).
- 5 days to implement non-UI functional parts.
- 3 days to implement the UI.
- Total: (1 + 1 + 5 + 3 days) * 2 for buffer = 20 days.


## 3. Project-wide Variables

Brecht made some interesting points:

- What are the concrete use cases for setting properties other than filepaths with project variables?  E.g. if it's basically just render settings, maybe that should just be handled as its own thing...?
  - Talking with the Blender studio artists: other than file paths, mainly just render settings.  Maybe some things like camera z location and focal length (e.g. for the current game project), but that could be done with drivers if we expose variables to drivers.
- Having each property define its own expression (in turn determining which variable(s) it uses) works when you know ahead of time all the properties you might want to influence.  But if later in production you realize you need to set another property project-wide...?  Or if a new property gets added that you also need to set project-wide?
  - Talking with the Blender studio artists: they agree there's a trade-off here, and they also don't know which way would be best.

I think a reasonable first iteration of this would be:

- Basic creation of project variables.
- Making them available to the file path system implemented in milestone 1.
- ~~Making them accessible from Python/RNA.~~
- ~~Making them available from drivers.~~

The design for just this is (probably?) pretty straightforward, and it handles a most of the use cases for variables.  Then we can look into how to handle applying the variables to non-animatable properties as a separate milestone, including figuring out the design.

(After discussion with others, we decided to leave off the last two items for the first iteration.)

### Conclusion

For the first iteration of Project Setup, we'll only support filepaths.  So this milestone is just allowing the user to define their own top-level variables that can be used within filepaths.  We can tackle additional use cases later.

### Time Estimate

- 2 days to implement non-UI functional parts.
- 2 days to implement UI.
- Total: (2 + 2 days) * 2 for buffer = 8 days.


## 4. Asset Management

I'm not familiar with the asset system code yet, so I'm not really sure how much work this will take.  Based on some conversations with Julian, I don't *think* it will be an undue amount of work, but I'll need to investigate to provide a reasonable estimate.

Needs investigation:
- How should this be designed on the code side?
- What are the actual steps needed to get this implemented?

### Time Guestimate

- 1 day for exploratory coding to figure things out.
- 3 days to implement non-UI functional parts.
- 2 days to implement UI.
- Total: (1 + 3 + 2 days) * 2 for buffer = 12 days.


## 5. Add-ons

This is a big unknown to me.  The basics of just allowing project-local add-ons will probably(?) be straightforward.  But project/user-level add-on *settings* aspect of this is one big unknown to me.

Needs investigation:
- How do add-on settings work, and how could we go about implementing the project-level and user-level settings in a reasonable way?

### Time Guestimate

For basic project-local add-ons:

- 1 day for exploratory coding to figure things out.
- 4 days to implement non-UI functional parts.
- 2 days to implement UI.
- Total: (1 + 4 + 2 days) * 2 for buffer = 14 days.

For project-specific and user-specific addon settings:

- 2 days for investigation, exploratory coding, and poking people who
know things, to figure out a good way to approach it.
- 6 days to implement (functionality & UI).
- Total: (2 + 6 days) * 2 for buffer = 16 days.

(Note that the settings stuff is the biggest unknown in the project
for me, so this estimate is the most fuzzy out of everything.)

- Total of both parts: 30 days


## 6. OCIO

Just a project-wide OCIO configuration.  Nothing fancy.

Needs investigation:
- How conventient is it to replace the current OCIO config at run time?  I suspect that the OCIO library itself probably makes this pretty easy, but I'm less sure about the Blender integration.

### Time Guestimate

- 2 days to investigate live re-loading of OCIO config in blender.
- 2 days to implement non-UI functional parts.
- Less than 1 day for UI.
- Total: (2 + 2 + 1 days) * 2 for buffer = 10 days.


## 7. Shot Variables

Idea is to be able to specify additional project variables via files in subdirectories of the project.  Similar to how `.gitignore` files work, where you can put them in any subdirectory to define additional ignores for that directory and its children.

Questions:

- Does it make sense to limit this to just variables?  What about being able to define anything that the project's top-level toml files can?
  - Could result in conflicts that are harder to do something sensible with than variables.  But if this is an advanced feature, maybe it can just be a "you're welcome to shoot yourself in the foot" kind of thing?
  - Christoph said they probably wouldn't use this at the studio he works for.  They don't tie this kind of thing to the directory hierarchy, and would rather specify this at the top level.  E.g. "for paths that match this expression, define/do these things".  So maybe keeping everything top-level but providing a way to limit to specified paths would be better?

Needs investigation:
- A good design for how this should actually work.

### Time Guestimate

We need to decide on the concrete design before I can break this down and make an estimate.