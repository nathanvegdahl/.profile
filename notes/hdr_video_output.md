# Saving HDR tonemapped images out of Blender

The key to doing this is to abuse input transforms as output transforms.  Create an input transform in OCIO that when reversed goes from the reference space to a tone mapped display-linear space.  You can then use this to save tonemapped EXR files from Blender.

# Converting dispay-linear EXRs to HDR video files with FFMPEG

```
ffmpeg -i input_exrs%04d.exr -r 24 -c:v libx265 -vf "zscale=rangein=full:range=full:primariesin=709:primaries=2020:matrixin=709:matrix=2020_ncl:transferin=linear:transfer=arib-std-b67" -color_trc arib-std-b67 -color_primaries bt2020 -colorspace bt2020nc -color_range 2 -pix_fmt yuv420p10 output_video_file.mkv
```

This requires ffmpeg to be built with the [zscale](https://ffmpeg.org/ffmpeg-filters.html#zscale-1) video filter.

The zscale filter parameters used are the following:
- `rangein=full` specifies that the input color value ranges are "full", meaning that, for example, zero means black.  This is in contrast to limited/broadcast color, where a small non-zero value is used to indicate black.
- `range=full` is the same, but for the output.
- `primariesin=709` and `matrixin=709` specifies that the input RGB primaries are Rec.709.
- `primaries=2020` and `matrix=2020_ncl` specifies the output RGB primaries as Rec.2020 and converts the input colors to that.  The `2020_ncl` is "Rec.2020 non-constant luminance", and is the required matrix for HDR on YouTube.
- `transferin=linear`  specifies that the input EXRs use a linear transfer function.  This is likely assumed anyway, but this makes it explicit.
- `transfer=arib-std-b67` converts the input values to use the HLG transfer function. `transfer=smpte2084` can be used instead for PQ.

To be a valid HDR video file, the video stream also needs metadata about the color space.  That metadata is provided by these parameters:

- `-color_trc arib-std-b67` tags the transfer function as HLG.  Use `smpte2084` instead for PQ.
- `-color_primaries bt2020` and `-colorspace bt2020nc` tags the color primaries as Rec.2020 non-constant luminance.
- `-color_range 2` tags the channel range as full, rather than limited.

The additional ffmpeg parameters:

- `-r 24` sets the frame rate of the output video.  In this case, 24 fps.
- `-pix_fmt yuv420p10` sets the pixel format to be 10 bits rather than 8 bits.

Parameters not included in the example ffmpeg command above, but that can also be provided:

- `-c:v libx265` to use the H.265/HEVC video codec.  This allows 12-bit color, rather than just 10-bit like H.264.
- `-c:v libaom` to use the AV1 video codec.  This also allows 12-bit color.
- With either of the above, to use 12-bit color with this codec, additionally pass `-pix_fmt yuv420p12`
 instead of `-pix_fmt yuv420p10` as in the example command.
 
 For example, this gives 12-bit H.265:
 
 ```
ffmpeg -i input_exrs%04d.exr -r 24 -c:v libx265 -vf "zscale=rangein=full:range=full:primariesin=709:primaries=2020:matrixin=709:matrix=2020_ncl:transferin=linear:transfer=arib-std-b67" -color_trc arib-std-b67 -color_primaries bt2020 -colorspace bt2020nc -color_range 2 -c:v libx265 -pix_fmt yuv420p12 output_video_file.mkv
```

Same but with PQ instead of HLG:

```
ffmpeg -i %04d.exr -r 24 -c:v libx265 -vf "zscale=rangein=full:range=full:primariesin=709:primaries=2020:matrixin=709:matrix=2020_ncl:transferin=linear:transfer=smpte2084" -color_trc smpte2084 -color_primaries bt2020 -colorspace bt2020nc -color_range 2 -c:v libx265 -pix_fmt yuv420p12 output_video_file.mkv
```
