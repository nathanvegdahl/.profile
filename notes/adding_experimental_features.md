https://developer.blender.org/docs/features/interface/experimental_features/

# Runtime enable/disable

Add the user-togglable flag:

1. Add a field to `UserDef_Experimental` in `makesdna/DNA_userdef_types.h`, somewhere *after* the `SANITIZE_AFTER_HERE` field.
2. Wrap it as an RNA bool property in the function `rna_def_userdef_experimental()` in `makesrna/intern/rna_userdef.cc`
3. Add that to the `draw()` method of either `USERPREF_PT_experimental_new_features` or `USERPREF_PT_experimental_prototypes` in `space_userprefs.py`.  Note that this includes providing a link to a url about the feature/prototype, and a corresponding label for that link.  Follow the examples already there.

To then check at runtime if your flag is enabled/disabled, use the `USER_EXPERIMENTAL_TEST` macro like this:

```
if USER_EXPERIMENTAL_TEST(&U, your_flag) {
    // ...
}
```

You could also check the flag directly (e.g. via `U.experimental.your_flag`) .  However, it's better to use the macro because it does some additional checks, which can then be maintained in one place for all experimental features.

From Python, you can also access the flag in the Context:

```
if context.preferences.experimental.your_flag:
    # ...
```

# Compile-time enable/disable

Run-time checks should be preferred when feasible, but in some cases you may want to do things like remove access to RNA APIs, which can only be done at compile time.

For such cases, you can define a CMake flag for your feature like so:

```
if(WITH_EXPERIMENTAL_FEATURES)
  add_definitions(-DWITH_YOUR_FEATURE)
endif()
```

And then use that with the usual ifdefs in the C/C++ code:

```
#ifdef WITH_YOUR_FEATURE
// ...
#endif
```
